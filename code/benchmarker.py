import numpy as np
import timeit
import copy
import pandas as pd
import time


def benchmark(sorting_algorithm, name, name_element_ordering, exponent, number_of_executions):

    print(time.strftime("%H:%M:%S", time.localtime()) +
          ": starting " + name + "-" + name_element_ordering + "-" + str(exponent))  # print start time

    number_of_elements = 2**exponent
    np.random.seed(1337)
    test_data = []

    if name_element_ordering == "reverse":
        test_data = np.arange(1, number_of_elements +
                              1)[::-1]  # REVERSE sorted data
    elif name_element_ordering == "sorted":
        test_data = np.arange(1, number_of_elements+1)  # SORTED data
    elif name_element_ordering == "random":
        test_data = np.random.random_sample(
            (number_of_elements,))  # RANDOM data
    else:
        print("Unknown name_element_ordering '"+name_element_ordering+"'. Benchmark for '" +
              name+"-"+name_element_ordering+"' not run...")
        return

    clock = timeit.Timer(stmt='sort_func(copy(data))',
                         globals={'sort_func': sorting_algorithm,
                                  'data': test_data,
                                  'copy': copy.copy})
    t = clock.repeat(repeat=5, number=number_of_executions)
    t = pd.DataFrame(t)
    t.to_pickle("../data/"+name+"-"+name_element_ordering+"-" +
                str(number_of_elements)+"-2^"+str(exponent)+".pkl")

    print(t)  # print dataframe containing average times per test round

    print(time.strftime("%H:%M:%S", time.localtime()) +
          ": finished " + name + "-" + name_element_ordering + "-" + str(exponent))  # print end time
