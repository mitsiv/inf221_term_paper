import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

# # configuration
# sorting_algorithms = [
#     "bubblesort",
#     "insertionsort",
#     "mergesort",
#     "numpysort",
#     "pythonsort",
#     "quicksort"
# ]  # name of algorithms

# sort_types = ["sorted", "reverse", "random"]

# # Create list with the number of elements for each benchmark
# data_size_exponents = [5, 8, 11, 14]


def plot(sorting_algorithm_names, sort_types, data_size_exponents):

    for x in sorting_algorithm_names:
        for y in sort_types:
            time_list = []  # Create list that will hold time for benchmarks
            std_list = []  # Create list that will hold std of time for benchmarks
            for z in data_size_exponents:
                times = pd.read_pickle("../data/"+x+"-"+y
                                       + "-"+str(2**z)+"-2^"+str(z)+".pkl")
                times_array = times.values
                time = np.mean(times_array)
                std = np.std(times_array)
                time_list.append(time)
                std_list.append(std)
            plt.errorbar([2**z for z in data_size_exponents],
                         time_list, std_list, label=x+"-"+y)

        plt.xscale("log", base=2)
        plt.yscale("log", base=10)

        plt.xlabel('Number of elements')
        plt.ylabel('Time in seconds (s)')
        plt.title(x)
        plt.legend()
        #plt.figure(figsize=(84/25.4, 55/25.4))
        plt.savefig("../figures/plot_"+x+".pdf")
        # plt.show()
        plt.clf()
