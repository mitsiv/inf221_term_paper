import sys
import benchmarker
import plot
# sorting algorithms
import sortingalgorithms.bubblesort as bubblesort
import sortingalgorithms.insertionsort as insertionsort
import sortingalgorithms.mergesort as mergesort
import sortingalgorithms.numpysort as numpysort
import sortingalgorithms.pythonsort as pythonsort
import sortingalgorithms.quicksort as quicksort

# configuration
sorting_algorithms = [
    {
        "algorithm": bubblesort.algorithm,
        "name": "bubblesort",
        "number_of_executions": 5
    },
    {
        "algorithm": insertionsort.algorithm,
        "name": "insertionsort",
        "number_of_executions": 5
    },
    {
        "algorithm": mergesort.algorithm,
        "name": "mergesort",
        "number_of_executions": 50
    },
    {
        "algorithm": numpysort.algorithm,
        "name": "numpysort",
        "number_of_executions": 50
    },
    {
        "algorithm": pythonsort.algorithm,
        "name": "pythonsort",
        "number_of_executions": 50
    },
    {
        "algorithm": quicksort.algorithm,
        "name": "quicksort",
        "number_of_executions": 50
    }
]

sort_types = ["sorted", "reverse", "random"]
# numbers that will be used as exponents to the number 2, to create test_data
data_size_exponents = [5, 8, 11, 14]

if len(sys.argv)-1 > 0 and sys.argv[1] == "--run-benchmark":
    print("Running benchmark...")
    # run benchmark for config
    for x in sorting_algorithms:
        for y in sort_types:
            for z in data_size_exponents:
                benchmarker.benchmark(x["algorithm"], x["name"], y,
                                      z, x["number_of_executions"])
    print("Done running benchmark.")
else:
    print("Did not run benchmark.")

print("Creating plots...")
plot.plot(
    [x["name"] for x in sorting_algorithms],
    sort_types,
    data_size_exponents
)
print("Done creating plots.")

print("Script finished!")
